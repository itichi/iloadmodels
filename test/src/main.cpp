#include <ILoadModels/Shader.hpp>
#include <ILoadModels/Model.hpp>

#include <iostream>

int main() {

    // TODO must add a OpenGL context..

    std::cout << "Loading model" << std::endl;
    ILoadModels::Model m("bunny.obj");
    std::cout << "Model Loaded" << std::endl;
    std::cout << "Compiling shaders" << std::endl;
    ILoadModels::Shader s("", "", nullptr, nullptr, nullptr, false);
    std::cout << "Shaders Compiled" << std::endl;

    m.draw(s);

    return 0;
}