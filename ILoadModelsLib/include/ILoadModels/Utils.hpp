#pragma once

#include <string>
#include <assimp/scene.h>

namespace ILoadModels {
	
    typedef unsigned int uint;
    typedef unsigned int gEnum;

	struct Vector2 {
		float x;
		float y;
    };
	
	struct Vector3 {
		float x;
		float y;
		float z;
    };
	
	struct Vertex {
		Vector3 position;
		Vector3 normal;
		Vector3 tangent;
		Vector3 bitangent;
		Vector2 textureCoordinates; 
    };
	
	struct Texture {
		uint id;
		std::string type;
		aiString path;
    };

    uint _textureFromFile(const char* path, std::string directory);

}