#pragma once

#include <assimp/scene.h>
#include <vector>

#include "Utils.hpp"
#include "ILoadModels/Shader.hpp"

namespace ILoadModels {

    class Mesh {
    public:
        Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
        ~Mesh();

        void draw(Shader shader);

        void _setupMesh();

        bool loadMesh(const char * fileName);
        void render();

    private:
        std::vector<Vertex> m_vertices;
        std::vector<unsigned int> m_indices;
        std::vector<Texture> m_textures;

        unsigned int m_VAO;
        unsigned int m_VBO;
        unsigned int m_EBO;
    };
}