#pragma once

#include "Shader.hpp"
#include "Mesh.hpp"
#include <vector>

namespace ILoadModels {
	class Model {
    public:
		Model(const char * path);

		void draw(Shader shader);
	
	private:
		void _loadModel(const std::string path);
		
		void _processNode(aiNode * node, const aiScene * scene);
		
		Mesh _processMesh(aiMesh * mesh, const aiScene * scene);
		
		std::vector<Texture> _loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
		
    private:
        std::vector<Mesh> m_meshes;

        std::string m_directory;

        std::vector<Texture> m_textures_loaded;
	};

};