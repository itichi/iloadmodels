#pragma once

#include <string>
#include "Utils.hpp"

namespace ILoadModels {
    class Shader {
    public:
        Shader(const char * mVertexPath,
            const char * mFragmentPath,
            const char * mGeometryPath,
            const char * mTessCPath,
            const char * mTessEPath,
            bool isPath = true);

        Shader(const char* mVertexPath,
            const char* mFragmentPath,
            bool isPath = true);

        const uint getProgram() {
            return m_id;
        }

    	const uint id()
        {
            return m_id;
        }

    private:
        void _checkCompileErrors(const uint &object, std::string type);

        std::string _shaderTypeToString(gEnum shaderType);

        uint _pathOrCodeToShader(const char* pathCode, gEnum shaderType, bool isPath);

        uint _compileShader(const char * mVertexPath,
            const char * mFragmentPath,
            const char * mGeometryPath,
            const char * mTessCPath,
            const char * mTessEPath,
            bool isPath);

    private:
        uint m_id;

    };
}