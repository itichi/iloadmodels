#include <ILoadModels/Utils.hpp>
#include <glad/glad.h>
//#define STB_IMAGE_IMPLEMENTATION
//#include <stb_image.h>
#include <stdexcept>

#include "../vendors/assimp/code/Pbrt/stb_image.h"

namespace ILoadModels {

	GLenum get_image_format(int comp)
	{
		GLenum format;
		if (comp == 1)
			format = GL_RED;
		else if (comp == 3)
			format = GL_RGB;
		else if (comp == 4)
			format = GL_RGBA;

		return format;
	}
	
	uint _textureFromFile(const char* path, std::string directory) {
			//Generate texture ID and load texture data 
			std::string filename = std::string(path);
			filename = directory + '/' + filename;
			uint textureID;
			glGenTextures(1, &textureID);
			int width, height, comp;
			unsigned char* image = stbi_load(filename.c_str(), &width, &height, &comp, 0);
			if (!image) {
				stbi_image_free(image);
				throw std::runtime_error(std::string("Image not found: ") + directory + " " + path + "\n");
			}
		
			// Assign texture to ID
			glBindTexture(GL_TEXTURE_2D, textureID);
			// TODO handle number of channels
			glTexImage2D(GL_TEXTURE_2D, 0, get_image_format(comp), width, height, 0, get_image_format(comp), GL_UNSIGNED_BYTE, image);
			glGenerateMipmap(GL_TEXTURE_2D);

			// Parameters
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0);
		
			stbi_image_free(image);
		
			return textureID;
	}
}
