#include <ILoadModels/Mesh.hpp>

#include <glad/glad.h>
#include <string>
#include <sstream>
#include <fstream>

namespace ILoadModels {

    Mesh::Mesh(std::vector<Vertex> vertices, std::vector<uint> indices, std::vector<Texture> textures)
        :
        m_vertices(vertices),
        m_indices(indices),
        m_textures(textures)
    {
        _setupMesh();
    }
    Mesh::~Mesh() {}

    void Mesh::draw(Shader shader) {
        uint diffuseNumber = 1;
        uint specularNumber = 1;

        for (size_t idx = 0; idx < m_textures.size(); idx++) {
            glActiveTexture(GL_TEXTURE0 + idx);
            std::stringstream ss;
            std::string number;
            std::string name = m_textures[idx].type;
            if (name == "texture_diffuse") {
                ss << diffuseNumber++;
            }
            else if (name == "texture_specular") {
                ss << specularNumber++;
            }
            number = ss.str();
            glUniform1i(glGetUniformLocation(shader.getProgram(),
                (name + number).c_str()), idx);
            glBindTexture(GL_TEXTURE_2D, m_textures[idx].id);
        }

        glUniform1f(glGetUniformLocation(shader.getProgram(),
            "material.shininess"), 16.0f); // TODO change it

        glBindVertexArray(m_VAO);
        glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        for (size_t idx = 0; idx < m_textures.size(); idx++) {
            glActiveTexture(GL_TEXTURE0 + idx);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }

    void Mesh::_setupMesh() {
        glGenVertexArrays(1, &m_VAO);
        glGenBuffers(1, &m_VBO);
        glGenBuffers(1, &m_EBO);

        glBindVertexArray(m_VAO);
        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
        glBufferData(GL_ARRAY_BUFFER,
            m_vertices.size() * sizeof(Vertex),
            &m_vertices[0],
            GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            m_indices.size() * sizeof(uint),
            &m_indices[0],
            GL_STATIC_DRAW);

        glVertexAttribPointer(0,
            3,
            GL_FLOAT,
            GL_FALSE,
            sizeof(Vertex),
            (GLvoid*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1,
            3,
            GL_FLOAT,
            GL_FALSE,
            sizeof(Vertex),
            (GLvoid*)offsetof(Vertex, normal));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2,
            2,
            GL_FLOAT,
            GL_FALSE,
            sizeof(Vertex),
            (GLvoid*)offsetof(Vertex, textureCoordinates));
        glEnableVertexAttribArray(2);

        glBindVertexArray(0);
    }

}