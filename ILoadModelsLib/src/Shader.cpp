#include <ILoadModels/Shader.hpp>

#include <glad/glad.h>
#include <iostream>
#include <sstream>
#include <fstream>

namespace ILoadModels {
    Shader::Shader(const char * vertexPath,
        const char * fragmentPath,
        const char * geometryPath,
        const char * tessCPath,
        const char * tessEPath,
        bool isPath) {
        m_id = _compileShader(vertexPath,
            fragmentPath,
            geometryPath,
            tessCPath,
            tessEPath,
            isPath);
    }

	Shader::Shader(const char* mVertexPath,
		const char* mFragmentPath,
		bool isPath)
			: Shader(mVertexPath, mFragmentPath,
				nullptr, nullptr, nullptr,
				isPath) {}
	
	void Shader::_checkCompileErrors(const uint &object, std::string type) {
		GLint success;
		char infoLog[1024];

		if (type != "Program") {
			glGetShaderiv(object, GL_COMPILE_STATUS, &success);
			if (!success) {
				glGetShaderInfoLog(object, 1024, NULL, infoLog);
				std::cout << "| ERROR::SHADER: Compile-time error: Type: " << type << std::endl;
				std::cout << infoLog << "\n -- --------------------------------------------------- -- \n";
			}
		}
		else {
			glGetProgramiv(object, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(object, 1024, NULL, infoLog);
				std::cout << "| ERROR::Shader: Link-time error: Type: " << type << std::endl;
				std::cout << infoLog << "\n -- --------------------------------------------------- -- \n";
			}
		}
	}

	std::string Shader::_shaderTypeToString(gEnum shaderType) {
		std::string ret = "";

		switch (shaderType) {
		case GL_COMPUTE_SHADER:
			ret = "Compute";
			break;
		case GL_VERTEX_SHADER:
			ret = "Vertex";
			break;
		case GL_TESS_CONTROL_SHADER:
			ret = "Tessellation Control";
			break;
		case GL_TESS_EVALUATION_SHADER:
			ret = "Tessellation Evaluation";
			break;
		case GL_GEOMETRY_SHADER:
			ret = "Geometry";
			break;
		case GL_FRAGMENT_SHADER:
			ret = "Fragment";
			break;
		default:
			ret = "unknown";
			break;
		}
		return ret;
	}

	uint Shader::_pathOrCodeToShader(const char* pathCode, gEnum shaderType, bool isPath) {
		std::string code;
		if (isPath) {
			std::ifstream shaderFile;
			std::stringstream shaderStream;

			shaderFile.open(pathCode);
			shaderStream << shaderFile.rdbuf();
			shaderFile.close();

			code = shaderStream.str();
		}
		else {
			code = pathCode;
		}

		const char * ccode = code.c_str();

		// Vertex Shader
		uint shader;
		shader = glCreateShader(shaderType);
		glShaderSource(shader, 1, &ccode, NULL);
		glCompileShader(shader);
		_checkCompileErrors(shader, _shaderTypeToString(shaderType));

		return shader;
	}

	uint Shader::_compileShader(const char * mVertexPath,
		const char * mFragmentPath,
		const char * mGeometryPath,
		const char * mTessCPath,
		const char * mTessEPath,
		bool isPath)
	{

		uint vertexShader = _pathOrCodeToShader(mVertexPath, GL_VERTEX_SHADER, isPath);
		uint fragmentShader = _pathOrCodeToShader(mFragmentPath, GL_FRAGMENT_SHADER, isPath);
		uint geometryShader = (mGeometryPath) ? _pathOrCodeToShader(mGeometryPath, GL_GEOMETRY_SHADER, isPath) : 0;
		uint tessCShader = (mTessCPath) ? _pathOrCodeToShader(mTessCPath, GL_TESS_CONTROL_SHADER, isPath) : 0;
		uint tessEShader = (mTessEPath) ? _pathOrCodeToShader(mTessEPath, GL_TESS_EVALUATION_SHADER, isPath) : 0;

		// Create Program
		uint ID = glCreateProgram();

		// Attach the shaders to the program
		glAttachShader(ID, vertexShader);
		glAttachShader(ID, fragmentShader);
		if (mGeometryPath) glAttachShader(ID, geometryShader);
		if (mTessCPath) glAttachShader(ID, tessCShader);
		if (mTessEPath) glAttachShader(ID, tessEShader);

		glLinkProgram(ID);
		_checkCompileErrors(ID, "Program");

		// Delete the shaders as they're linked into our program now and no longer necessery
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
		if (mGeometryPath) glDeleteShader(geometryShader);
		if (mTessCPath) glDeleteShader(tessCShader);
		if (mTessEPath) glDeleteShader(tessEShader);

		return ID;
	}
}