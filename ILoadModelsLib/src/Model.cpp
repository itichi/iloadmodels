#include <ILoadModels/Model.hpp>
#include <ILoadModels/Mesh.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>

namespace ILoadModels {
	Model::Model(const char * path) {
		_loadModel(path);
	}

	void Model::draw(Shader shader) {
		for (size_t idx = 0; idx < m_meshes.size(); idx++) {
			m_meshes[idx].draw(shader);
		}
	}
	
	void Model::_loadModel(const std::string path) {
		Assimp::Importer importer;
		const aiScene * scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
		
		if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
			throw std::runtime_error(std::string("ERROR::ASSIMP:: ") + importer.GetErrorString() + "\n");
		}
		
		m_directory = path.substr(0, path.find_last_of('/'));
		_processNode(scene->mRootNode, scene);
	}
	
	void Model::_processNode(aiNode * node, const aiScene * scene) {
		for (size_t idx = 0; idx < node->mNumMeshes; idx++) {
			aiMesh * mesh = scene->mMeshes[node->mMeshes[idx]];
			m_meshes.push_back(_processMesh(mesh, scene));
		}
		
		for (size_t idx = 0; idx < node->mNumChildren; idx++) {
			_processNode(node->mChildren[idx], scene);
		}
	}
	
	Mesh Model::_processMesh(aiMesh * mesh, const aiScene * scene) {
		std::vector<Vertex> vertices;
		std::vector<uint> indices;
		std::vector<Texture> textures;
		
		for (size_t idx = 0; idx < mesh->mNumVertices; idx++) {
			Vertex vertex;
			Vector3 vector;
			vector.x = mesh->mVertices[idx].x;
			vector.y = mesh->mVertices[idx].y;
			vector.z = mesh->mVertices[idx].z;
			
			vertex.position = vector;
			
			vector.x = mesh->mNormals[idx].x;
			vector.y = mesh->mNormals[idx].y;
			vector.z = mesh->mNormals[idx].z;
			
			vertex.normal = vector;
			
			if (mesh->mTextureCoords[0]) {
				Vector2 vec;
				vec.x = mesh->mTextureCoords[0][idx].x;
				vec.y = mesh->mTextureCoords[0][idx].y;
				vertex.textureCoordinates = vec;
				
				vector.x = mesh->mTangents[idx].x;
				vector.y = mesh->mTangents[idx].y;
				vector.z = mesh->mTangents[idx].z;

				vertex.tangent = vector;

				vector.x = mesh->mBitangents[idx].x;
				vector.y = mesh->mBitangents[idx].y;
				vector.z = mesh->mBitangents[idx].z;

				vertex.bitangent = vector;
			}
			else {
				vertex.textureCoordinates = Vector2{0.0f, 0.0f};
			}
			vertices.push_back(vertex);
		}
		
		for (size_t idx = 0; idx < mesh->mNumFaces; idx++) {
			aiFace face = mesh->mFaces[idx];
			for (size_t idy = 0; idy < face.mNumIndices; idy++) {
				indices.push_back(face.mIndices[idy]);
			}
		}
		
		if (mesh->mMaterialIndex >= 0) {
			aiMaterial * material = scene->mMaterials[mesh->mMaterialIndex];
			// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
            // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
            // Same applies to other texture as the following list summarizes:
            // Diffuse: texture_diffuseN
            // Specular: texture_specularN
            // Normal: texture_normalN

            // 1. Diffuse maps
            std::vector<Texture> diffuseMaps = _loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
            // 2. Specular maps
            std::vector<Texture> specularMaps = _loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
			// 3. normal maps
			std::vector<Texture> normalMaps = _loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
			textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
			// 4. height maps
			std::vector<Texture> heightMaps = _loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
			textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
		}
		
		return Mesh(vertices, indices, textures);
	}
	
	std::vector<Texture> Model::_loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
    {
        std::vector<Texture> textures;
        for(size_t idx = 0; idx < mat->GetTextureCount(type); idx++)
        {
            aiString str;
            mat->GetTexture(type, idx, &str);
            // Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
            bool skip = false;
            for(size_t idy = 0; idy < m_textures_loaded.size(); idy++)
            {
                if(std::strcmp(m_textures_loaded[idy].path.C_Str(), str.C_Str()) == 0)
                {
                    textures.push_back(m_textures_loaded[idy]);
                    skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
                    break;
                }
            }
            if(!skip)
            {   // If texture hasn't been loaded already, load it
                Texture texture;
                texture.id = _textureFromFile(str.C_Str(), m_directory);
                texture.type = typeName;
                texture.path = str;
                textures.push_back(texture);
                m_textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
            }
        }
        return textures;
    }

};